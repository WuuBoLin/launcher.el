;;; launcher.el --- Launch commands or applications from Emacs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 WuuBoLin
;;
;; Author: WuuBoLin <me@wuubolin.dev>
;; Maintainer: WuuBoLin <me@wuubolin.dev>
;; Created: May 10, 2024
;; Modified: May 10, 2024
;; Version: 0.0.1
;; Homepage: https://gitlab.com/WuuBoLin/launcher.el
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Description:
;;
;; launcher define the `launch' command which uses
;; Emacs standard completion feature to select a command or an application installed
;; on your machine and launch it.
;;
;;; Code:
(require 'xdg)
(require 'cl-seq)

(defcustom launcher-apps-directories
  (mapcar (lambda (dir) (expand-file-name "applications" dir))
	  (cons (xdg-data-home)
		(xdg-data-dirs)))
  "Directories in which to search for applications (.desktop files)."
  :type '(repeat directory))

(defcustom launcher--annotation-function #'launcher--annotation-function-default
  "Define the function that genereate the annotation for each completion choices."
  :type 'function)

(defcustom launcher--action-function #'launcher--action-function-default
  "Define the function that is used to run the selected application."
  :type 'function)

(defvar launcher--cache nil
  "Cache of desktop files data.")

(defvar launcher--cache-timestamp nil
  "Time when we last updated the cached application list.")

(defvar launcher--cached-files nil
  "List of cached desktop files.")

(defun launcher-list-desktop-files ()
  "Return an alist of all Linux applications.
Each list entry is a pair of (desktop-name . desktop-file).
This function always returns its elements in a stable order."
  (let ((hash (make-hash-table :test #'equal))
	result)
    (dolist (dir launcher-apps-directories)
      (when (file-exists-p dir)
	(let ((dir (file-name-as-directory dir)))
	  (dolist (file (directory-files-recursively dir ".*\\.desktop$"))
	    (let ((id (subst-char-in-string ?/ ?- (file-relative-name file dir))))
	      (when (and (not (gethash id hash)) (file-readable-p file))
		(push (cons id file) result)
		(puthash id file hash)))))))
    result))

(defun launcher-list-bin-files ()
  "Return an alist of all executables in directories listed in `exec-path'.
Each entry is a pair of (binary-name . binary-file)."
  (let (result)
    (dolist (dir exec-path)
      (when (file-directory-p dir)
        (dolist (file (directory-files dir t nil t))
          (when (and (file-executable-p file)
                     (not (file-directory-p file)))
            (push (cons (file-name-nondirectory file) file) result)))))
    result))

(defun launcher-parse-desktop-files (files)
  "Parse the .desktop files to return usable informations."
  (let ((hash (make-hash-table :test #'equal)))
    (dolist (entry files hash)
      (let ((file (cdr entry)))
	(with-temp-buffer
	  (insert-file-contents file)
	  (goto-char (point-min))
	  (let ((start (re-search-forward "^\\[Desktop Entry\\] *$" nil t))
		(end (re-search-forward "^\\[" nil t))
		(visible t)
		name comment exec)
	    (catch 'break
	      (unless start
		(message "Warning: File %s has no [Desktop Entry] group" file)
		(throw 'break nil))

	      (goto-char start)
	      (when (re-search-forward "^\\(Hidden\\|NoDisplay\\) *= *\\(1\\|true\\) *$" end t)
		(setq visible nil))
	      (setq name (match-string 1))

	      (goto-char start)
	      (unless (re-search-forward "^Type *= *Application *$" end t)
		(throw 'break nil))
	      (setq name (match-string 1))

	      (goto-char start)
	      (unless (re-search-forward "^Name *= *\\(.+\\)$" end t)
		(message "Warning: File %s has no Name" file)
		(throw 'break nil))
	      (setq name (match-string 1))

	      (goto-char start)
	      (when (re-search-forward "^Comment *= *\\(.+\\)$" end t)
		(setq comment (match-string 1)))

	      (goto-char start)
	      (unless (re-search-forward "^Exec *= *\\(.+\\)$" end t)
		;; Don't warn because this can technically be a valid desktop file.
		(throw 'break nil))
	      (setq exec (match-string 1))

	      (goto-char start)
	      (when (re-search-forward "^TryExec *= *\\(.+\\)$" end t)
		(let ((try-exec (match-string 1)))
		  (unless (locate-file try-exec exec-path nil #'file-executable-p)
		    (throw 'break nil))))

	      (puthash name
		       (list (cons 'file file)
			     (cons 'exec exec)
			     (cons 'comment comment)
			     (cons 'visible visible))
		       hash))))))))

(defun launcher-list-entries ()
  "Return list of all commands and .desktop applications."
  (let* ((new-desktop-alist (launcher-list-desktop-files))
         (new-bin-alist (launcher-list-bin-files))
         (new-files (append (mapcar 'cdr new-desktop-alist) (mapcar 'cdr new-bin-alist))))
    (unless (and (equal new-files launcher--cached-files)
		 (null (cl-find-if
			(lambda (file)
			  (time-less-p
			   launcher--cache-timestamp
			   (nth 5 (file-attributes file))))
			new-files)))
      ;; Reset a new launcher--cache
      ;; Add desktop files first
      (setq launcher--cache (launcher-parse-desktop-files new-desktop-alist))
      ;; Then add binaries
      (dolist (bin new-bin-alist)
        (let* ((file (car bin))
               (exec (cdr bin))
               (entry (list (cons 'file file)
                            (cons 'exec exec)
                            (cons 'comment "")
                            (cons 'visible t))))
          (puthash file entry launcher--cache)))
      (setq launcher--cache-timestamp (current-time))
      (setq launcher--cached-files new-files)))
  launcher--cache)

(defun launcher--annotation-function-default (choice)
  "Default function to annotate the completion choices."
  (let ((str (cdr (assq 'comment (gethash choice launcher--cache)))))
    (when str (concat " - " (propertize str 'face 'completions-annotations)))))

(defun launcher--action-function-default (selected)
  "Default function used to run the selected application."
  (let* ((exec (cdr (assq 'exec (gethash selected launcher--cache))))
	 (command (let (result)
		    (dolist (chunk (split-string exec " ") result)
		      (unless (or (equal chunk "%U")
				  (equal chunk "%F")
				  (equal chunk "%u")
				  (equal chunk "%f"))
			(setq result (concat result chunk " ")))))))
    (if (eq system-type 'gnu/linux)
	(start-process-shell-command command nil (concat "setsid " command))
      (call-process-shell-command command nil 0 nil))))

;;;###autoload
(defun launch (&optional arg)
  "Launch a command or an application."
  (interactive)
  (let* ((candidates (launcher-list-entries))
	 (result (completing-read
		  "Launch: "
		  (lambda (str pred flag)
		    (if (eq flag 'metadata)
			'(metadata
			  (annotation-function . (lambda (choice)
						   (funcall
						    launcher--annotation-function
						    choice))))
		      (complete-with-action flag candidates str pred)))
		  (lambda (x y)
		    (if arg
			t
		      (cdr (assq 'visible y))))
		  t nil 'launcher nil nil)))
    (funcall launcher--action-function result)))

;; Provide the launcher feature
(provide 'launcher)
;;; launcher.el ends here
